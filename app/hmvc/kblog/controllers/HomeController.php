<?php

namespace App\Hmvc\Kblog\Controllers;

use App\Hmvc\Kblog\Models\Post;

class HomeController extends \BaseController {

	public function index()
	{
		$posts = Post::all();

		return \View::make('kblog::index')
			->with('titulo_do_blog',\Config::get('kblog::meuconfig.titulo'))
			->with('posts',$posts);
	}

	public function post($slug)
	{
		return \View::make('kblog::post')
			->with('titulo_do_blog',\Config::get('kblog::meuconfig.titulo'))
			->with('slug',$slug);
	}

}