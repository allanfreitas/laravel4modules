<?php namespace App\Hmvc\Kblog;
 
class ServiceProvider extends \App\Hmvc\ServiceProvider {
 
    public function register()
    {
        parent::register('kblog');
    }
 
    public function boot()
    {
        parent::boot('kblog');
    }
 
}