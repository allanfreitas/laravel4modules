<?php

Route::get('blog/{slug}',array('as'=>'kblog_post_view',
	'uses' => 'App\Hmvc\Kblog\Controllers\HomeController@post'));

Route::get('blog', 'App\Hmvc\Kblog\Controllers\HomeController@index');
