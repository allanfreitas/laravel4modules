
<h1>Home - Listagem | {{$titulo_do_blog}}</h1>

<ul>
@foreach($posts as $post)
	<li><a href="{{URL::route('kblog_post_view',$post['slug'])}}">{{$post['titulo']}}</a></li>
@endforeach
</ul>