<?php

return array(
	'titulo' => 'Meu Blog HMVC',
	'posts' => array(
		array(
			'slug' => 'como-instalar-laravel-4',
			'titulo' => 'Como Instalar Laravel 4'
		),
		array(
			'slug' => 'composer-openssl-error',
			'titulo' => 'Composer Openssl Error'
		)
	),
);