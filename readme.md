Projeto Laravel 4 Modules
=========

Projeto de Teste para mostrar (uma das formas) como usar HMVC no Laravel4.


##Alterações na Instalação Padrão do Laravel4 

* Adicionei a pasta "hmvc" ao composer.json

```php
    "autoload": {
		"classmap": [
			"app/commands",
			"app/controllers",
			"app/models",
			"app/database/migrations",
			"app/database/seeds",
			"app/tests/TestCase.php",
			"app/hmvc"
		]
	},
```


* Adicionei o meu modulo na configuração de Providers
no arquivo (app/config/app.php);

```php
//Meu Modulo Kblog
'App\Hmvc\Kblog\ServiceProvider',
```

* O Restante foi tudo código "NOVO" dentro da pasta "app/hmvc".


##Who I'm
  [@allanfreitas]: http://twitter.com/allanfreitas
  [www.allanfreitas.com.br]: http://allanfreitas.com.br